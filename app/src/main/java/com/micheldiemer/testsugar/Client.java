package com.micheldiemer.testsugar;


import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

import java.util.Date;

/**
 * Created by m.dimer on 14/11/2017.
 */

@Table
public class Client extends SugarRecord {

    /*
     * Données ne pouvant être modifiées cf service commercial/financier
     */

    @Unique
    long identifiant;

    @Unique
    String str_identifiant;

    // l'annotation @Column permet de donner le nom de la colonne dans la base de données SQLite
    @Column(name="nom",unique=false,notNull=true)
    String nom;

    String prenom,adresse,codePostal,ville,telephone;
    String idCompteur;
    Double ancienReleve;
    Date dateAncienReleve;


    /*
     * Données à saisir
     */
    Double dernierReleve;
    Date dateDernierReleve;
    String signatureBase64;
    int situation;
    /* Exemples de situation client ------
     *  0 client non traité par défaut
     *  1 Absent
     *  2 Absent mais relevé possible sans signature client
     *  3 Present, relevé ok mais pas de signature car pas représentant légal
     *  4 present et tout ok
     *  5 déménagé / logement vide
     *  6 démanagé / nouveaux locataires
     *  72,73,74 idem 2,3,4 mais dysfonctionnement
     *  82,83,84 idem 2,3,4 mais dysfonctionnement / dégradation
     */




    public Client() {
    }

    public Client(long identifiant, String nom, String prenom, String adresse, String codePostal, String ville, String telephone, String idCompteur, Double ancienReleve, Date dateAncienReleve) {
        this.identifiant = identifiant;
        this.str_identifiant = String.valueOf(identifiant);
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.codePostal = codePostal;
        this.ville = ville;
        this.telephone = telephone;
        this.idCompteur = idCompteur;
        this.ancienReleve = ancienReleve;
        this.dateAncienReleve = dateAncienReleve;

        this.dernierReleve = 0D;
        this.dateDernierReleve = new Date();
        this.signatureBase64 = "";
        this.situation = 0;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public long getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(long identifiant) {
        this.identifiant = identifiant;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getIdCompteur() {
        return idCompteur;
    }

    public void setIdCompteur(String idCompteur) {
        this.idCompteur = idCompteur;
    }

    public Double getAncienReleve() {
        return ancienReleve;
    }

    public void setAncienReleve(Double ancienReleve) {
        this.ancienReleve = ancienReleve;
    }

    public Date getDateAncienReleve() {
        return dateAncienReleve;
    }

    public void setDateAncienReleve(Date dateAncienReleve) {
        this.dateAncienReleve = dateAncienReleve;
    }

    public Double getDernierReleve() {
        return dernierReleve;
    }

    public void setDernierReleve(Double dernierReleve) {
        this.dernierReleve = dernierReleve;
    }

    public Date getDateDernierReleve() {
        return dateDernierReleve;
    }

    public void setDateDernierReleve(Date dateDernierReleve) {
        this.dateDernierReleve = dateDernierReleve;
    }

    public String getSignatureBase64() {
        return signatureBase64;
    }

    public void setSignatureBase64(String signatureBase64) {
        this.signatureBase64 = signatureBase64;
    }

    public int getSituation() {
        return situation;
    }

    public void setSituation(int situation) {
        this.situation = situation;
    }
}
