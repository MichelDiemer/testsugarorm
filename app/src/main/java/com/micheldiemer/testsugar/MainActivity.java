package com.micheldiemer.testsugar;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.orm.SugarContext;
import com.orm.SugarDb;

import java.lang.reflect.Field;
import java.sql.Array;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.orm.SugarContext.getSugarContext;

/**
 * Projet d'exemple d'utilisation de SugarOrm
 *      * Ajouter dans build.gradle (Module;app) dans dependencies
 *      *   implementation 'com.github.satyan:sugar:1.5'
 *      * Ajouter dans AndroidManofext.xml dans <application>
 *      *   <meta-data android:name="DATABASE" android:value="sugar_example.db" />
 *      *   <meta-data android:name="VERSION" android:value="1" />
 *      *   <meta-data android:name="QUERY_LOG" android:value="true" />
 *      *  Créer une classe (par exemple Client)
 *      *    L'annotation @Unique sert à identifier les champs Unique
 *      *    Il faut un constructeur vide et des champs publics
 *      *
 *
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // on récupère la TextView
        TextView tv = findViewById(R.id.textView);

        // On supprime tous les clienst de la base de données
        Client.deleteAll(Client.class);

        // On créé les clients dans la Base de Données
        creerClientsBDD();

        // On récupère la liste des clietns sous forme de chaîne de caractères
        String str =  listerClientsBDD();
        str = str + "\n\n" + sugarORMFind();
        //str = str + "\n\n" + structureSugarDb();

        // On affiche la liste des clients
        tv.setText(str);


    }


    public static void creerClientsBDD() {
        Client c;


        // Suppression de tous les clients de la base de données
        Client.deleteAll(Client.class);

        // Création d'un objet Client
        c = new Client(1001,"NOM1","PRENOM1","adresse","67000","Strasbourg","0300000000","ABCDEFGHI",0.0,new Date());
        // Enregistrement dans la base de données
        c.save();

        // Création d'un objet Client
        c = new Client(1002,"NOM2","PRENOM2","adresse","67000","Strasbourg","0300000000","IHGFEDCBA",0.0,new Date());
        // Enregistrement dans la base de données
        c.save();
    }


    // Illustre la méthode find de SugarOrm
    public String sugarORMFind() {
        StringBuilder str = new StringBuilder();
        List<Client> listeClients;
        Client clientSelectionne;



        // Renvoie un seul client d'après un critère

        try {
            str.append("Find : Clients à vérifier (situation==0)\n");
            listeClients = Client.find(Client.class, "situation=?", "0");
            for(Client c: listeClients) {
                str.append("Nouveau client : " + c.getNom() + " " + c.getPrenom() + "\n");
            }
        } catch(Exception e) { str.append("exception:" + e.getCause()+" "+e.getMessage()); }

        str.append("\n");
        try {
            str.append("Find : Identifiant. Le client doit exister.\n");

            // find renvoie une liste de clients
            // Noter le .get(0) à la fin pour n'aovir qu'un seul clietn
            clientSelectionne = Client.find(Client.class,"identifiant=?","1002").get(0);
            str.append(clientSelectionne.getId()+" "+clientSelectionne.getIdentifiant()+" "+clientSelectionne.getNom()+" "+clientSelectionne.getPrenom()+"\n");
        } catch(Exception e) { str.append("recherche identifiant " + e.getCause()+" "+e.getMessage()); }

        return str.toString();
    }


    public String listerClientsBDD() {
        // Un objet client de la liste
        Client c;

        // Le nombre total de clients
        int nombre_clients = 0;

        // On construit la liste des clients (StringBuilder plus efficace que String)
        StringBuilder str = new StringBuilder();

        str.append("Liste de tous les clients de la base de données SQLite\n");

        // Créer un objet Iterator permettant de lister les clients
        Iterator<Client> itClients = Client.findAll(Client.class);

        // On sélectionne les clients les uns après les autres jusqu'au dernier
        while(itClients.hasNext()) {
            // Récupération d'un objet client de la BDD
            c = itClients.next();
            // Ajout du client à la chaine de caractères avec des espaces devant et un saut de ligne derrière
            str.append("     "+c.getId()+" "+c.getIdentifiant()+" "+c.getNom()+" "+c.getPrenom()+"\n");
            // Augmentation du nombre ce clients
            nombre_clients++;
        }

        // On précise le nombre de clients trouvés
        str.append("     ");
        str.append(nombre_clients);
        str.append(" client(s).");

        return str.toString();
    }

    // Renvoie la structure de la base de données interne
    public String structureSugarDb() {
        StringBuilder str = new StringBuilder();

        str.append("Structure de la base de données SQLite\n");
        try {
            // Récupération d'un objet SQLiteDatabase en utilisant l'introspection des classes
            SugarContext obj =getSugarContext();
            Field f = obj.getClass().getDeclaredField("sugarDb");
            f.setAccessible(true);
            //récupération de l'attribut sugarDB'
            SugarDb sugarDb= (SugarDb)f.get(obj);
            SQLiteDatabase sqLiteDatabase = sugarDb.getDB();


            // Requête SQL permettant de lister les tables sqlite
            // et la requête SQL de créaztion de la table
            String sql = "SELECT name, sql FROM sqlite_master WHERE type='table';";

            // Parcours du curseur
            Cursor cursor = sqLiteDatabase.rawQuery(sql,null);
            if(cursor!=null) {
                while(cursor.moveToNext()) {
                    str.append("\n");
                    str.append(cursor.getString(0) + "\n");
                    str.append(cursor.getString(1) + "\n");
                }
            }
        } catch (Exception e) {
            str.append("structureSugarDb "+ e.getCause()+ " " + e.getMessage());
        }
        str.append("\n");
        return str.toString();
    }
}
